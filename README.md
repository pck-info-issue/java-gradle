# Scope

This project 
* is used to reproduce an issue with package-info files and annotation processors
* is part of the GitLab Group: https://gitlab.com/pck-info-issue
* uses the `jar` file generated by the [color-lib](https://gitlab.com/pck-info-issue/color-lib) project (it is in the `libs/` directory).
* like the [color-lib](https://gitlab.com/pck-info-issue/color-lib) project, it 
  * contains only a single enum `com.example.BoolEnum` 
  in the **same** package that is used by `ColorEnum` of the [color-lib](https://gitlab.com/pck-info-issue/color-lib) project
  * it uses the same [EnumMapper](https://github.com/tmtron/enum-mapper) annotation processor
* this project also has a `package-info.java` file in `com.example` (which causes the problem) 
  
## Issue  
When you build the project the annotation processor will create a file `com.example.BoolEnum_MapperFull.java`
in `build/generatedclasses/main/com/example/`, which is expected.

The problem is, that it will also create the file `com.example.ColorEnum_MapperFull.java`:
* this file should **not** be created, because this project does not even have it's source code
* the `com.example.ColorEnum_MapperFull.class` file **is** already contained in `libs/color-1.0-SNAPSHOT.jar`  
and thus, we end up with the same class file twice!

## Workaround
The problem is related to the `package-info.java` file in the `com.example` package.  
Ways to avoid the problem:
* delete `package-info.java`
* or comment out it's contents

Then `com.example.ColorEnum_MapperFull.java` will not be generated twice.

## Notes
The issue is not related to the gradle build system:
* see https://gitlab.com/pck-info-issue/java-only which uses only `javac`  
* and it is also not related to the annotation-processor:  
it can also be reproduced with other annotation processors: e.g. https://immutables.github.io/ 

## Build

To build the project and generate the jar file:

* Windows: `gradlew.bat clean jar`
* Linux:   `gradlew clean jar`

