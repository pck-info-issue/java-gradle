package com.example;

import com.tmtron.enums.EnumMapper;

@EnumMapper
public enum BoolEnum {
    TRUE, FALSE
}